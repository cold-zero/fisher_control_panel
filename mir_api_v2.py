import requests
import json
import pprint
import time


class MIRApi:
	def __init__(self):
		self.authKey = "Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA=="


	def request(self, url_request):
		"""
		Aici va veni un banc
		"""
		url_base = "http://mir.com/api/v2.0.0"
		url = url_base + url_request
		return url
		

	def readRegister(self, registerNo):
		url = self.request("/registers/%i" % registerNo)
		head = {"Content-Type": "application/json",
		"Authorization": self.authKey,
		"Accept-Language": "en_US"}
		
		try:
			req = requests.get(url, headers=head)
		except requests.exceptions.ConnectionError as e:
			print(e)
			returnValue = {"conn_status":"CONN-ERROR"}
			return returnValue

		if req.status_code == 200:
			returnValue = req.json()
		elif req.status_code == 400:
			returnValue = {"conn_status":req.status_code, "description":"Bad request error"}
		elif req.status_code == 404:
			returnValue = {"conn_status":req.status_code, "description":"Path not found"}
		else:
			returnValue = {"conn_status":req.status_code, "description":"Unknow error"}
		
		return returnValue


	def readAllRegisters(self):
		url = self.request("/registers")
		head = {"Content-Type": "application/json",
		"Accept-Language": "en_US",
		"Authorization": self.authKey}
		
		try:
			req = requests.get(url, headers=head)
		except requests.exceptions.ConnectionError as e:
			print(e)
			returnValue = {"conn_status":"CONN-ERROR"}
			return returnValue

		if req.status_code == 200:
			returnValue = req.json()
		elif req.status_code == 400:
			returnValue = {"conn_status":req.status_code, "description":"Bad request error"}
		elif req.status_code == 404:
			returnValue = {"conn_status":req.status_code, "description":"Path not found"}
		else:
			returnValue = {"conn_status":req.status_code, "description":"Unknow error"}

		return returnValue


	def setRegister(self, registerNo, registerValue):
		url = self.request("/registers/%i" % registerNo)
		head = {"Content-Type": "application/json",
			"Authorization": self.authKey,
			"Accept-Language": "en_US"}
		body = json.dumps({"value": registerValue,
			"label": ""})
		
		try:
			req = requests.put(url, headers=head, data=body)
		except requests.exceptions.ConnectionError as e:
			print(e)
			returnValue = {"conn_status":"CONN-ERROR"}
			return returnValue

		if req.status_code == 200:
			returnValue = req.json()
		elif req.status_code == 400:
			returnValue = {"conn_status":req.status_code, "description":"Bad request error"}
		elif req.status_code == 404:
			returnValue = {"conn_status":req.status_code, "description":"Path not found"}
		else:
			returnValue = {"conn_status":req.status_code, "description":"Unknow error"}
		
		return returnValue


	def addMissionToQueue(self, mission_id):
		url = self.request("/mission_queue")
		head = {"Content-Type": "application/json",
			"Authorization": self.authKey,
			"Accept-Language": "en_US"}
		body = json.dumps({"mission_id": mission_id,
			"message": "Starting mission...",
			"priority": 1})
		
		try:
			req = requests.post(url, headers=head, data=body)
		except requests.exceptions.ConnectionError as e:
			print(e)
			returnValue = {"conn_status":"CONN-ERROR"}
			return returnValue

		if req.status_code == 201:
			returnValue = req.json()
		elif req.status_code == 400:
			returnValue = {"conn_status":req.status_code, "description":"Bad request error"}
		elif req.status_code == 404:
			returnValue = {"conn_status":req.status_code, "description":"Path not found"}
		else:
			returnValue = {"conn_status":req.status_code, "description":"Unknow error"}
		
		return returnValue

	def getStatus(self):
		url = self.request("/status")
		head = {"Content-Type": "application/json",
			"Authorization": self.authKey,
			"Accept-Language": "en_US"}

		try:
			req = requests.get(url, headers=head)
		except requests.exceptions.ConnectionError as e:
			print(e)
			returnValue = {"conn_status":"CONN-ERROR"}
			return returnValue

		if req.status_code == 200:
			returnValue = req.json()
		elif req.status_code == 400:
			returnValue = {"conn_status":req.status_code, "description":"Bad request error"}
		elif req.status_code == 404:
			returnValue = {"conn_status":req.status_code, "description":"Path not found"}
		else:
			returnValue = {"conn_status":req.status_code, "description":"Unknow error"}
		
		return returnValue

	def setRobotReady(self):
		url = self.request("/status")
		head = {"Content-Type": "application/json",
			"Authorization": self.authKey,
			"Accept-Language": "en_US"}

		body = json.dumps({"state_id":3})

		try:
			req = requests.put(url, headers=head, data=body)
		except requests.exceptions.ConnectionError as e:
			print(e)
			returnValue = {"conn_status":"CONN-ERROR"}
			return returnValue

		if req.status_code == 200:
			returnValue = req.json()
		elif req.status_code == 400:
			returnValue = {"conn_status":req.status_code, "description":"Bad request error"}
		elif req.status_code == 404:
			returnValue = {"conn_status":req.status_code, "description":"Path not found"}
		else:
			returnValue = {"conn_status":req.status_code, "description":"Unknow error"}
		
		return returnValue

	def clearErrors(self):
		url = self.request("/status")
		head = {"Content-Type": "application/json",
			"Authorization": self.authKey,
			"Accept-Language": "en_US"}

		body = json.dumps({"clear_error": True})

		try:
			req = requests.put(url, headers=head, data=body)
		except requests.exceptions.ConnectionError as e:
			print(e)
			returnValue = {"conn_status":"CONN-ERROR"}
			return returnValue

		if req.status_code == 200:
			returnValue = req.json()
		elif req.status_code == 400:
			returnValue = {"conn_status":req.status_code, "description":"Bad request error"}
		elif req.status_code == 404:
			returnValue = {"conn_status":req.status_code, "description":"Path not found"}
		else:
			returnValue = {"conn_status":req.status_code, "description":"Unknow error"}
		
		return returnValue

	def abortAllMissions(self):
		url = self.request("/mission_queue")
		head = {"Content-Type": "application/json",
			"Authorization": self.authKey,
			"Accept-Language": "en_US"}

		try:
			req = requests.delete(url, headers=head)
		except requests.exceptions.ConnectionError as e:
			print(e)
			returnValue = {"conn_status":"CONN-ERROR"}
			return returnValue

		if req.status_code == 200:
			returnValue = req.json()
		elif req.status_code == 400:
			returnValue = {"conn_status":req.status_code, "description":"Bad request error"}
		elif req.status_code == 404:
			returnValue = {"conn_status":req.status_code, "description":"Path not found"}
		else:
			returnValue = {"conn_status":req.status_code, "description":"Unknow error"}
		
		return returnValue