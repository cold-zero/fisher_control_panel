import pygame

class TXGui:
	def __init__(self):
		self.mousePos = None
		self.lmbUP = False
		self._screen = None
		self.gray = (131,139,139)
		self.black = (0,0,0)
		self.white = (255,255,255)
		self.red = (200,0,0)
		self.darkseagreen = (180,238,180)
		self.consolegreen = (102,205,0)
		
	def Init(self, screen):
		self._screen = screen
		self.mousePos = pygame.mouse.get_pos() #initialise the var

	def Update(self):
		self.mousePos = pygame.mouse.get_pos()
		
	def CleanEvents(self):
		self.lmbUP = False

	def LMBClickUp(self):
		self.lmbUP = True

	def TextObj(self, text, font, color):
		textSurf = font.render(text, True, color)
		return textSurf, textSurf.get_rect()

	def TextShadow(self, text, font, x, y, w, h):
		color_gray = (131, 139, 139)
		shadowSurf, shadowRect = self.TextObj(text, font, color_gray)
		shadowRect.center = ( (x+(w/2)) + 2, (y+(h/2)) + 2 )
		return shadowSurf, shadowRect

	def Label(self, sText, x, y, font = None, fontsize = 20, fontcolor = (255, 255, 255)):
		if font == None:
			font = pygame.font.SysFont(pygame.font.get_default_font(), fontsize, False, False)

		textSurf, textRect = self.TextObj(sText, font, fontcolor)
		textRect = textRect.move(x, y)
		self._screen.blit(textSurf, textRect)

	def ButtonImg(self, sText, x, y, w, h, img, imgrect, callback = None, font = None, fontsize = 20, fontcolor = (255, 255, 255)):
		if font == None:
			font = pygame.font.SysFont(pygame.font.get_default_font(), fontsize, False, False)
		imgrect = imgrect.move(x, y)

		if x+w > self.mousePos[0] > x and y+h > self.mousePos[1] > y:
			#do hover stuff
			shadowSurf, shadowRect = self.TextShadow(sText, font, x, y, w, h)
			self._screen.blit(shadowSurf, shadowRect)
			self._screen.blit(img, imgrect)
			if self.lmbUP and callback != None:
				callback()
				self.lmbUP = False
		else:
			self._screen.blit(img, imgrect)

		textSurf, textRect = self.TextObj(sText, font, fontcolor)
		textRect.center = ( (x+(w/2)), (y+(h/2)) )
		self._screen.blit(textSurf, textRect)

	def Button(self, sText, x, y, w, h, color, hoverColor, callback = None, font = None, fontsize = 28, fontcolor = (255, 255, 255)):
		if font == None:
			font = pygame.font.SysFont(pygame.font.get_default_font(), fontsize, False, False)

		if x+w > self.mousePos[0] > x and y+h > self.mousePos[1] > y:
			#do hover stuff
			pygame.draw.rect(self._screen, hoverColor, (x,y,w,h))
			if self.lmbUP and callback != None:
				callback()
				self.lmbUP = False
		else:
			pygame.draw.rect(self._screen, color, (x,y,w,h))

		textSurf, textRect = self.TextObj(sText, font, fontcolor)
		textRect.center = ( (x+(w/2)), (y+(h/2)) )
		self._screen.blit(textSurf, textRect)
		
	"""
	def ButtonTerm(self, sText, x, y, w, h, color, hoverColor, callback = None, font = None, fontsize = 20, fontcolor = self.white):
		if font == None:
			font = pygame.font.SysFont(pygame.font.get_default_font(), fontsize, False, False)

		if x+w > self.mousePos[0] > x and y+h > self.mousePos[1] > y:
			#do hover stuff
			pygame.draw.rect(self._screen, hoverColor, (x,y,w,h), 2)
			if self.lmbUP and callback != None:
				callback()
				self.lmbUP = False
		else:
			pygame.draw.rect(self._screen, color, (x,y,w,h), 2)

		textSurf, textRect = self.TextObj(sText, font, color)
		textRect.center = ( (x+(w/2)), (y+(h/2)) )
		self._screen.blit(textSurf, textRect)
	"""
	
	def AppCard(self, bgColor):
		pygame.draw.rect(self._screen, self.gray, (20, 20, 790, 470))
		pygame.draw.rect(self._screen, bgColor, (10, 10, 780, 460))
		
	def GroupFrame(self, x,y,w,h, borderColor):
		pygame.draw.rect(self._screen, borderColor, (x,y,w,h), 2)

	def GroupFrameFilled(self, x,y,w,h, borderColor, bgColor):
		pygame.draw.rect(self._screen, borderColor, (x,y,w,h), 2)
		pygame.draw.rect(self._screen, bgColor, (x+2,y+2,w-3,h-3))

	#TODO: create a gui Edit for text output
	def EditFrame(self, sText, x, y, w, h, font = None, fontsize = 28, fontcolor = (255, 255, 255)):
		pass