#!/usr/bin/python3

import pprint, time, os, platform, logging, pygame

from txgui import TXGui
from homescreen import HomeScreen


class App:
	def __init__(self):
		log("-> __init__")
		self._running = True
		self._screen = None
		self.size = self.wight, self.height = 800, 480
		self._appState = 0
		self._screenEvent = 0 #0 - null, 1 - back, 2 - home, 3 - chg screen, 10 - QUIT
		self.currentScreen = 'home'
		self.lastScreen = 'home'
		self.newScreen = 'home'


	def on_init(self):
		log("-> on_init")
		pygame.init()
		if platform.system() == "Windows":
			self._screen = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
		elif platform.system() == "Linux":
			self._screen = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.FULLSCREEN)
			#self._screen = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
		self._running = True
		
		for item in screen:
			screen[item].Init(self._screen, self.size)


	def on_event(self, event):
		if event.type == pygame.QUIT:
			self._running = False
		
		screen[self.currentScreen].OnEvent(event)


	def on_loop(self):
		global timeLast
		self._screen.fill([255, 255, 255])

		if self._screenEvent == 10:
				self._running = False
		elif self._screenEvent == 1:
			temp = self.currentScreen
			screen[self.lastScreen].ResetScreen()
			self.currentScreen = self.lastScreen
			self.lastScreen = temp
		elif self._screenEvent == 2:
			screen['home'].ResetScreen()
			self.lastScreen = self.currentScreen
			self.currentScreen = 'home'
		elif self._screenEvent == 3:
			self.lastScreen = self.currentScreen
			screen[self.newScreen].ResetScreen()
			self.currentScreen = self.newScreen

		(self._screenEvent, self.newScreen) = screen[self.currentScreen].Update()

		pygame.display.update()


	def on_cleanup(self):
		log("-> on_cleanup")
		pygame.display.quit()
		pygame.quit()

	def on_execute(self):
		if self.on_init() == False:
			self._running = False

		while (self._running):
			for event in pygame.event.get():
				self.on_event(event)
			self.on_loop()
		self.on_cleanup()


if __name__ == "__main__":

	debugEnabled = True

	def log(sText):
		global debugEnabled
		if debugEnabled:
			logging.debug(sText)


	screen = {'home':HomeScreen()}

	if platform.system() == "Windows":
		logging.basicConfig(filename='walker-logs.txt', level=logging.DEBUG)
	elif platform.system() == "Linux":
		logging.basicConfig(filename='/home/pi/gui-logs.txt', level=logging.DEBUG)


	theApp = App()
	theApp.on_execute()