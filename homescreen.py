import pygame, time
from txgui import TXGui
import platform
import subprocess
import pprint
from decimal import *

from mir_api_v2 import MIRApi

### FOR DEBUGING ##############
import logging

debugEnabled = False

def log(sText):
	global debugEnabled
	if debugEnabled:
		logging.debug(sText)
###############################

TWOPLACES = Decimal(10) ** -2

class HomeScreen:
	def __init__(self):
		self.gui = TXGui()
		self.scrSize = self.scrWidth, self.scrHeight = 0, 0
		self._event = 0
		self.nextScreen = 'null'
		self.updateStatusTime = 0
		self.checkCharge = False
		self.checkChargeTime = 0
		self.registerBatteryTime = 0
		
		#MIR API stuff
		self.mirAPI = MIRApi()

		self.mirUVRegister = 3
		self.mirBlowerRegister = 2
		self.mirChargeRegister = 4
		
		self.mirMissionText = "none"
		self.mirBattery = 0 # In percentage
		self.mirBatteryTimeRemaining = 0 # In seconds
		self.mirStateText = "none"
		self.mirPosX = 0
		self.mirPoxY = 0
		self.mirOrientation = 0
		self.mirModeId = 7
		self.mirStateId = 5 # 3 - Ready, 4 - Paused, 5 - Executing
		self.mirUptime = 0
		self.mirSpeed = 0
		self.mirErrors = None
		self.mirSessionGuid = "731fabb3-e862-11e8-b966-b8aeed71a487" # hardcoded

		self.mirBatteryOld = 0
		self.shouldCharge = False
		self.apiStatuslabel = "Enable REST"
		self.mirConnLabel = "offline"
		self.missionList = {
			"GO_CHARGE":"30cfe0ee-77fb-11ea-8fc6-b8aeed71a487",
			"CLEAN_UP_MISSION":"a6d16d28-77fa-11ea-8fc6-b8aeed71a487"
			}
		self.mapList = {
			"VB_CULOAR_ET1":"a1fbaa3f-77f4-11ea-8fc6-b8aeed71a487"
			}
		
		
		#Console output window
		self.consoleOut = ["", "", "", ""]
		self.consoleLine = 0

		if platform.system() == "Windows":
			logging.basicConfig(filename='walker-logs.txt', level=logging.DEBUG)
			self.font_path = "Comic Sans MS"
			self.wallpaperPath = "ui_background.jpg"
		elif platform.system() == "Linux":
			logging.basicConfig(filename='/home/pi/walker-logs.txt', level=logging.DEBUG)
			self.font_path = "Comic Sans MS"
			self.wallpaperPath = "/home/pi/fisher_control_panel/ui_background.jpg"


	def Init(self, screen, screenSize):
		self._screen = screen
		self.gui.Init(self._screen)
		self.scrSize = self.scrWidth, self.scrHeight = screenSize[0], screenSize[1]
		
		self._bgimg = pygame.image.load(self.wallpaperPath)
		self._bgrect = self._bgimg.get_rect()
		self.timeLast = time.time()
		self.updateStatusTime = time.time()
		self.ShowLoading()
		self.UpdateMirStatus()


	def OnEvent(self, event):
		if event.type == pygame.QUIT:
			self._event = 10
		elif event.type == pygame.MOUSEBUTTONUP:
			if event.button == 1:
				self.gui.LMBClickUp()


	def ResetScreen(self):
		self._event = 0
		self.nextScreen = 'null'


	def Update(self):
		self.gui.Update()
		self._screen.blit(self._bgimg, self._bgrect)

		if time.time() - self.updateStatusTime > 2:
			self.UpdateMirStatus() # Get the status of the MiR robot
			self.updateStatusTime = time.time()
		
		if self.mirStateId == 3 or self.mirStateId == 4:
			if self.checkCharge:
				if time.time() - self.checkChargeTime > 180:
					print("Checking battery charge (%s)" % self.mirBatteryOld)
					self.LogToConsole("Checking battery charge (%s)" % self.mirBatteryOld)
					if self.mirBatteryOld > self.mirBattery:
						self.Mission_GoCharge()
						print("Executing GO_CHARGE mission with battery %s" % self.mirBattery)
						self.checkCharge = False
						self.registerBatteryTime = time.time()
					else:
						self.checkCharge = False
						self.checkChargeTime = time.time()
						self.registerBatteryTime = time.time()
			else:
				if time.time() - self.registerBatteryTime > 4:
					print("Registering battery charge (%s)" % self.mirBattery)
					self.mirBatteryOld = self.mirBattery
					self.checkCharge = True
					self.checkChargeTime = time.time()



		# The UI is drawn here
		# ======================================================================================================
		#self.gui.GroupFrameFilled(0, 0, self.scrWidth, 100, self.gui.consolegreen, self.gui.consolegreen)
		self.gui.Label(self.consoleOut[0], 22, 12, fontcolor=self.gui.white, fontsize=24)
		self.gui.Label(self.consoleOut[1], 22, 32, fontcolor=self.gui.white, fontsize=24)
		self.gui.Label(self.consoleOut[2], 22, 52, fontcolor=self.gui.white, fontsize=24)
		self.gui.Label(self.consoleOut[3], 22, 72, fontcolor=self.gui.white, fontsize=24)

		self.gui.Label("MIR is %s" % self.mirConnLabel, 395, 130, fontcolor=self.gui.darkseagreen, fontsize=24)
		self.gui.Label("State: {}".format(self.mirStateText), 395, 152, fontsize=24)

		self.gui.Label("MiR battery: {}%".format(self.mirBattery), 395, 190, fontsize=24)
		uptime = self.mirUptime / 60
		self.gui.Label("Uptime: {} minutes".format(Decimal(uptime).quantize(TWOPLACES)), 395, 212, fontsize=24)
		remainingTime = self.mirBatteryTimeRemaining / 60
		self.gui.Label("Remaining time: {} minutes".format(Decimal(remainingTime).quantize(TWOPLACES)), 395, 234, fontsize=24)

		self.gui.Label("MiR Speed: %s" % self.mirSpeed, 395, 273, fontsize=24)
		

		self.gui.Label("Missions", 22, 130, fontsize=24)
		self.gui.Button("Clean-up", 20, 170, 220, 70, self.gui.darkseagreen, self.gui.red, self.Mission_CleanUp, fontcolor=self.gui.black)
		self.gui.Button("Go charge", 20, 250, 220, 70, self.gui.darkseagreen, self.gui.red, self.Mission_GoCharge, fontcolor=self.gui.black)
		self.gui.Button("STOP", 20, 350, 220, 70, self.gui.darkseagreen, self.gui.red, self.Mission_Stop, fontcolor=self.gui.black)
		self.gui.Button("Turn off UV", 270, 350, 220, 70, self.gui.darkseagreen, self.gui.red, self.TurnOffUV, fontcolor=self.gui.black)
		self.gui.Button("Turn on UV", 523, 350, 220, 70, self.gui.darkseagreen, self.gui.red, self.TurnOnUV, fontcolor=self.gui.black)
		
		#self.gui.Button("Settings", 10, 255, 150, 75, self.gui.darkseagreen, self.gui.red, self.ChangeScreen, fontcolor=self.gui.black)

		self.gui.Button("", self.scrWidth-30, self.scrHeight - 30, 30, 30, self.gui.black, self.gui.black, self.ExitApp)

		self.gui.CleanEvents()
		# ======================================================================================================
		return self._event, self.nextScreen



	def ShowLoading(self):
		self._screen.blit(self._bgimg, self._bgrect)
		self.gui.Label("Loading...", 300, 220, fontsize=56)
		pygame.display.update()
		self.gui.CleanEvents()

	def UpdateMirStatus(self):
		#print("Updating MIR connection status...")
		resp = self.mirAPI.getStatus()
		if 'conn_status' in resp:
			#self.LogToConsole("MiR REST API is not available")
			self.mirConnLabel = "offline"
		else:
			# No we get all the data needed
			self.mirConnLabel = "online"
			self.mirBattery = Decimal(resp['battery_percentage']).quantize(TWOPLACES)
			self.mirBatteryTimeRemaining = resp['battery_time_remaining']
			self.mirStateText = resp['state_text']
			self.mirPosX = resp['position']['x']
			self.mirPosY = resp['position']['y']
			self.mirOrientation = resp['position']['orientation']
			self.mirModeId = resp['mode_id']
			self.mirStateId = resp['state_id']
			self.mirUptime = resp['uptime']
			self.mirSpeed = resp['velocity']['linear']
			self.mirErrors = resp['errors']
		#print("Status updated")

	def Mission_CleanUp(self):
		if self.mirBattery > 30:
			print("Adding CLEAN_UP mission to queue...")
			self.mirAPI.abortAllMissions()
			self.mirAPI.clearErrors()
			resp = self.mirAPI.addMissionToQueue(self.missionList['CLEAN_UP_MISSION'])
			if 'conn_status' in resp:
				#self.LogToConsole(resp['conn_status'])
				self.LogToConsole("Could not execute. AGV not connected.")
			else:
				for mission_name, mission_key in self.missionList.items():
					if mission_key == resp['mission_id']:
						self.LogToConsole("Running mission %s" % mission_name)
			self.mirAPI.setRobotReady()
		else:
			self.LogToConsole("Battery too low!")

	def Mission_GoCharge(self):
		print("Adding GO_CHARGE mission to queue...")
		self.mirAPI.abortAllMissions()
		self.mirAPI.clearErrors()
		resp = self.mirAPI.addMissionToQueue(self.missionList['GO_CHARGE'])
		if 'conn_status' in resp:
			#self.LogToConsole(resp['conn_status'])
			self.LogToConsole("Could not execute. AGV not connected.")
		else:
			for mission_name, mission_key in self.missionList.items():
				if mission_key == resp['mission_id']:
					self.LogToConsole("Running mission %s" % mission_name)
					self.checkCharge = False
					self.registerBatteryTime = time.time()
		self.mirAPI.setRobotReady()

	def Mission_Stop(self):
		print("Stopping robot")
		self.mirAPI.abortAllMissions()
		self.mirAPI.clearErrors()
		#if 'conn_status' in resp:
		#	self.LogToConsole("Could not execute. AGV not connected")
		#else:
		self.mirAPI.setRegister(self.mirUVRegister, 0)
		self.mirAPI.setRegister(self.mirBlowerRegister, 0)
		self.mirAPI.clearErrors()
		self.LogToConsole("AGV stopped!")

	def TurnOffUV(self):
		resp = self.mirAPI.setRegister(self.mirUVRegister, 0)
		if 'conn_status' in resp:
			self.LogToConsole("Could not execute. AGV not connected.")
		else:
			self.LogToConsole("UV lights off!")

	def TurnOnUV(self):
		resp = self.mirAPI.setRegister(self.mirUVRegister, 1)
		if 'conn_status' in resp:
			self.LogToConsole("Could not execute. AGV not connected.")
		else:
			self.LogToConsole("UV lights on!")

	def ChangeScreen(self):
		self._event = 3
		self.nextScreen = 'settings'

	def LogToConsole(self, sText):
		self.consoleOut[self.consoleLine] = sText
		self.consoleLine += 1
		if self.consoleLine > 3:
			self.consoleLine = 0

	def ExitApp(self):
		self._event = 10